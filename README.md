# Resharper 9 and StyleCop settings #

This repository exists to share ReSharper and StyleCop settings amongst projects. Simply clone and

1. Drop the StyleCop settings file into the root of your solution
2. Import the ReSharper.DotSettings file into ThisComputer (and save as a Team Shared DotSettings file)

More information can be found [on this blog post.](http://forloop.co.uk/blog/resharper-and-semanticmerge-equals-streamlined-development) 

### Contribution guidelines ###

If you spot any inconsistencies between the DotSettings file and StyleCop, or something is missing from either of the settings file, feel free to send me a pull request.

### Who do I talk to? ###

Please feel free to [send me a message on twitter (@forloop)](http://twitter.com/forloop)